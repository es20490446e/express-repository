#! /bin/bash

pkgname="autofirma"
RepoName="clienteafirma"

epoch=100
pkgrel=1
arch=("any")

pkgdesc="Permite firmar documentos en las webs de la administración pública española"
license=("GPL" "custom:EUPL")
url="https://github.com/ctt-gob-es/clienteafirma"
JavaVersion=11


pkgver="$(
	repoInfo tags "${url}" |
	grep "^v" |
	tr -d "v" |
	grep --invert-match "[[:alpha:]]" |
	head -n1
)"


makedepends=(
	"git"
	"jdk${JavaVersion}-openjdk"
	"maven"
	"so"
)


source=(
	"git+${url}#tag=v${pkgver}"
	"git+https://github.com/ctt-gob-es/clienteafirma-external"
	"git+https://aur.archlinux.org/autofirma.git"
)


sha1sums=(
	"SKIP"
	"SKIP"
	"SKIP"
)


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


build () {
	setGlobals
	BuildLibs
	BuildProgram
}


setGlobals () {
	PATH="/usr/lib/jvm/java-${JavaVersion}-openjdk/bin/:${PATH}"
	export PATH
}


BuildLibs () {
	cd "${srcdir}/clienteafirma-external"
	so mvn clean install -Dmaven.test.skip=true
}


BuildProgram () {
	cd "${srcdir}/${RepoName}"
	so mvn clean install -Denv=devel -Dmaven.test.skip=true
	so mvn clean install -Denv=install -Dmaven.test.skip=true
}


package () {
	depends+=("jre${JavaVersion}-openjdk")
	PackageFiles
}


PackageFiles () {
	so install -Dm644 \
		"${RepoName}/afirma-simple/target/AutoFirma.jar" \
		"${pkgdir}/usr/share/java/autofirma/autofirma.jar"

	so install -Dm755 \
		"autofirma/autofirma" \
		"${pkgdir}/usr/bin/autofirma"

	so install -Dm644 \
		"autofirma/autofirma.js" \
		"${pkgdir}/usr/lib/firefox/defaults/pref/autofirma.js"

	so install -Dm644 \
		"autofirma/autofirma.svg" \
		"${pkgdir}/usr/share/pixmaps/autofirma.svg"

	so install -Dm644 \
		"autofirma/autofirma.desktop" \
		"${pkgdir}/usr/share/applications/autofirma.desktop"

	so install -Dm644 \
		"autofirma/eupl-1.1.txt" \
		"${pkgdir}/usr/share/licenses/autofirma/EUPL"
}
