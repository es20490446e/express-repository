#! /bin/bash

pkgname="c++utilities"

epoch=100
pkgrel=1
arch=("x86_64")

pkgdesc="Common C++ classes and routines such as argument parser, IO and conversion utilities"
url="https://github.com/Martchus/cpp-utilities"
license=("GPL")


pkgver="$(
	repoInfo tags "${url}" |
	cut --delimiter='v' --fields=2 |
	head -n1
)"


source=("git+${url}")
sha256sums=("SKIP")


makedepends=(
	cmake
	git
	ninja
)


checkdepends=(
	cppunit
)


depends=(
	gcc-libs
)


build () {
	local Tags=("$(PreviousVersion)" "${pkgver}")
	local Tag

	for Tag in "${Tags[@]}"; do
		BuildTag "${Tag}"
	done
}


BuildTag () {
	local Tag="${1}"
	local Lock="${srcdir}/v${Tag}.lock"

	if [[ ! -f "${Lock}" ]]; then
		CreateTagDir "${Tag}"

		so cmake \
			-G Ninja \
			-D BUILD_SHARED_LIBS:BOOL=ON \
			-D CMAKE_BUILD_TYPE:STRING="Release" \
			-D CMAKE_INSTALL_PREFIX:PATH="/usr" \
			-D CONFIGURATION_NAME="$(TagMajorVersion "${Tag}")" \
			.

		ninja
		touch "${Lock}"
	fi
}


CreateTagDir () {
	local Tag="${1}"

	so rm --recursive --force "${srcdir}/v${Tag}"
	so cp --recursive "${srcdir}/cpp-utilities" "${srcdir}/v${Tag}"
	cd "${srcdir}/v${Tag}"
	so git checkout "tags/v${Tag}"
}


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


ninja () {
	local Error

	#shellcheck disable=SC2068
	if ! Error="$(/usr/bin/ninja ${@})"; then
		echo "-${FUNCNAME[1]} ${*}" >&2
		echo "${Error}" >&2
		false
	fi
}


PreviousVersion () {
	local PreviousMajorVersion; PreviousMajorVersion="$(("$(TagMajorVersion "${pkgver}")" -1))"

	Tags |
	grep "^${PreviousMajorVersion}" |
	tail -n1
}


TagMajorVersion () {
	local Tag="${1}"

	echo "${Tag}" |
	cut --delimiter='.' --fields=1
}


Tags () {
	cd "${srcdir}/cpp-utilities"

	git tag |
	cut --delimiter='v' --fields=2- |
	sort --version-sort
}


package () {
	PackageTags "$(PreviousVersion)" "${pkgver}"
	AppendLibArrays
}


PackageTags () {
	local Tags=("${@}")
	local Tag

	for Tag in "${Tags[@]}"; do
		PackageTag "${Tag}"
	done
}


PackageTag () {
	local Tag="${1}"

	cd "${srcdir}/v${Tag}"
	export DESTDIR="${pkgdir}"
	ninja install
}


AppendLibArrays () {
	AppendLibProvides
	AppendLibDepends
}


AppendLibProvides () {
	local LibProvides; readarray -t LibProvides < <(slibs inDir "${pkgdir}")
	provides+=("${LibProvides[@]}")
}


AppendLibDepends () {
	local LibDepends; readarray -t LibDepends < <(slibs inPackages "${depends[@]}")
	depends+=("${LibDepends[@]}")
}
