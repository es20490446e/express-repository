#! /bin/bash

post_install () {
	restartPolkit
}

post_upgrade () {
	restartPolkit
}

post_remove () {
	restartPolkit
	so rm --recursive --force "usr/share/calamares"
}

restartPolkit () {
	if systemctl status polkit &> /dev/null; then
		so systemctl restart polkit
	fi
}
