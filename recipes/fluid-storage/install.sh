#! /bin/bash


post_install () {
	SetCommitInterval
	EnableFastCommit
	ReloadVirtualMemorySettings
	EnablePeriodicTrim
}


post_upgrade () {
	post_install
}


so () {
	if [[ -x /bin/so ]]; then
		/bin/so "-${FUNCNAME[1]}" "${@}"
	else
		"${@}"
	fi
}


EnableFastCommit () {
	local Uuid
	local Device

	while read -r Uuid; do
		Device="/dev/disk/by-uuid/${Uuid}"

		if [[ -L "${Device}" ]]; then
			so tune2fs -O fast_commit "${Device}"
		fi
	done < <(Ext4Uuids)
}


EnablePeriodicTrim () {
	systemctl enable --now fstrim.timer
}


Ext4Uuids () {
	local Conf="/etc/fstab"

	if [[ -f "${Conf}" ]]; then
		grep ext4 "${Conf}" |
		grep UUID |
		cut --delimiter='=' --fields=2 |
		cut --delimiter=' ' --fields=1
	fi
}


ReloadVirtualMemorySettings () {
	so sysctl --quiet --load=/etc/sysctl.d/10-vm-cache.conf
}


SetCommitInterval () {
	local Conf="/etc/fstab"
	local Interval=30

	if [[ -f "${Conf}" ]]; then
		sed \
			-e "s|commit=[[:digit:]]*,|commit=${Interval},|g" \
			-e "s|commit=[[:digit:]]* |commit=${Interval} |g" \
			--in-place "${Conf}"
	fi
}
