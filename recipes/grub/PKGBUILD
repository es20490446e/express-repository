#! /bin/bash

pkgname="grub"
install="${pkgname}.install"

epoch=100
pkgrel=3
arch=("x86_64")

pkgdesc="Boots operating systems"
url="https://git.savannah.gnu.org/cgit/grub.git"
license=("GPL")
options=("!buildflags")


Tags=(); readarray -t Tags < <(
	repoInfo tags "${url}" |
	grep "^grub-"
)


Version="$(
	echo "${Tags[0]}" |
	cut --delimiter='-' --fields=2
)"


Release="$(
	echo "${Tags[@]}" |
	tr ' ' '\n' |
	grep -e "-${Version}" |
	wc -l
)"


pkgver="${Version}.${Release}"


backup=(
	"etc/default/grub"
    "etc/grub.d/40_custom"
)


makedepends=(
	autogen
	git
	help2man
	python
	rsync
	ttf-dejavu
)


depends=(
	dosfstools
	efibootmgr
	freetype2
	fuse3
	libisoburn
	libusb
	lzop
	mtools
	os-prober
	otf-unifont
	sdl12-compat
	sdl2-compat
	so
	sudo
)


Patches=(
	colors.patch
	dropins.patch
	fgrep.patch
	initramfs.patch
	quick.patch
	quiet.patch
)


source=(
	"git+https://git.savannah.gnu.org/git/grub.git#tag=${Tags[0]}"
    "git+https://git.savannah.gnu.org/git/gnulib.git"
    "config.hook"
    "font.hook"
    "sbat.csv.in"
    "grub.default"
    "update-grub"
    "${Patches[@]}"
)


b2sums=(); readarray -t b2sums < <(
	for Item in "${source[@]}"; do
		echo SKIP
	done
)


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


prepare() {
	cd "${srcdir}/grub"

	ApplyPatches
	FixConfigs
	GetLanguageFiles
	Bootstrap
}


ApplyPatches () {
	local Patch

	for Patch in "${Patches[@]}"; do
		so patch --forward --strip=1 --input="${srcdir}/${Patch}"
	done
}


FixConfigs () {
	sed \
		-e 's|=GNU/Linux|=Linux|' \
		-e 's|} GNU/Linux|}|' \
		-e 's| ro | rw |g' \
		-i "util/grub.d/10_linux.in"
}


GetLanguageFiles () {
	so ./linguas.sh
	sed "1i /^PO-Revision-Date:/ d" --in-place po/*.sed
}


Bootstrap () {
	so ./bootstrap \
		--gnulib-srcdir="${srcdir}/gnulib" \
		--no-git
}


build () {
	BuildPlatform "pc" "i386"
	BuildPlatform "efi" "i386"
	BuildPlatform "efi" "x86_64"
	BuildPlatform "emu" "x86_64"
}


BuildPlatform () {
	local Platform="${1}"
	local Arch="${2}"
	local Options=("${@:3}")
	local Lock="${srcdir}/${Platform}-${Arch}.lock"

	if [[ ! -f "${Lock}" ]]; then
		ChangeToBlankBuildDir "${Platform}" "${Arch}"
		Configure "${Platform}" "${Arch}"
		/bin/so "-${Platform}-${Arch}" make
		touch "${Lock}"
	fi
}


ChangeToBlankBuildDir () {
	local Platform="${1}"
	local Arch="${2}"

	so rm --recursive --force "${srcdir}/grub-${Platform}-${Arch}"
	so cp --recursive "${srcdir}/grub" "${srcdir}/grub-${Platform}-${Arch}"
	cd "${srcdir}/grub-${Platform}-${Arch}"
}


Configure () {
	local Platform="${1}"
	local Arch="${2}"
	local Options=("${@:3}")

	so ./configure \
		PACKAGE_VERSION="${epoch}:${pkgver}-${pkgrel}" \
		FREETYPE="pkg-config freetype2" \
		BUILD_FREETYPE="pkg-config freetype2" \
		--with-platform="${Platform}" \
		--target="${Arch}" \
		--with-dejavufont="/usr/share/fonts/TTF/DejaVuSansMono.ttf" \
		--with-unifont="/usr/share/fonts/unifont/unifont.otf" \
		--program-prefix="" \
		--with-grubdir="grub" \
		--with-bootdir="/boot" \
		--sysconfdir="/etc" \
		--prefix="/usr" \
		--bindir="/usr/bin" \
		--sbindir="/usr/bin" \
		--datarootdir="/usr/share" \
		--infodir="/usr/share/info" \
		--mandir="/usr/share/man" \
		--enable-quick-boot	\
		--enable-quiet-boot \
		--disable-libzfs \
		--disable-werror \
		"${Options[@]}"
}


package () {
	PackagePlatforms
	RemoveDebugFiles
	PackageIndividualFiles
}


PackagePlatforms () {
	local Platform

	while read -r Platform; do
		cd "${srcdir}/grub-${Platform}"
		so make DESTDIR="${pkgdir}" bashcompletiondir="/usr/share/bash-completion/completions" install
	done < <(Platforms)
}


Platforms () {
	cd "${srcdir}"

	find . -maxdepth 1 -name "grub-*" |
	cut --delimiter="-" --fields=2-
}


RemoveDebugFiles () {
	local File

	while read -r File; do
		so rm "${File}"
	done < <(
		find "${pkgdir}/usr/lib/grub" \
			-name "*.image" \
			-or -name "*.module" \
			-or -name "gdb_grub" \
			-or -name "gmodule.pl" \
			-or -name "kernel.exec"
	)
}


PackageIndividualFiles () {
	so install -Dm0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"
	so install -Dm644 "${srcdir}/config.hook" "${pkgdir}/usr/share/libalpm/hooks/99-grub-config.hook"
	so install -Dm644 "${srcdir}/font.hook" "${pkgdir}/usr/share/libalpm/hooks/grub-font.hook"
	so install -Dm755 "${srcdir}/update-grub" "${pkgdir}/usr/bin/update-grub"
	sed "s|%PKGVER%|${pkgver}-${pkgrel}|" < "${srcdir}/sbat.csv.in" > "${pkgdir}/usr/share/grub/sbat.csv"
}
