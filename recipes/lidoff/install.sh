#! /bin/bash


post_install () {
	systemctl enable --now lidoff
}


post_upgrade () {
	systemctl daemon-reload
}


pre_remove () {
	systemctl disable --now lidoff
}
