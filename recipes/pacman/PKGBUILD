#! /bin/bash

pkgname="pacman"
groups=("base-devel")

epoch=101
pkgrel=4
arch=("x86_64")

pkgdesc="Manages software packages"
license=("GPL")
install="install.sh"

url="https://gitlab.archlinux.org/pacman/pacman"
PackageUrl="https://gitlab.archlinux.org/archlinux/packaging/packages/pacman"


makedepends=(
	asciidoc
	doxygen
	git
	meson
	so
)


depends=(
	base
	base-devel-meta
	curl
	gnupg
	gpgme
	libarchive
	openssl
	pacman-contrib
	pacman-mirrorlist
	perl-locale-gettext
	systemd
)


optdepends=(
	"pacman-auto-update: Keeps the system automatically updated"
	"manjaro-keyring: Enables installing packages on Manjaro"
)


checkdepends=(
	fakechroot
	python
)


backup=(
	etc/makepkg.conf
	etc/makepkg.conf.d/fortran.conf
	etc/makepkg.conf.d/rust.conf
	etc/pacman.conf
)


options=(
	strip
)


Tags=(); readarray -t Tags < <(
	repoInfo tags "${url}" |
	cut --delimiter='v' --fields=2- |
	grep --invert-match "[[:alpha:]]" |
	head -n2
)


pkgver="${Tags[0]}"


source=(); readarray -t source < <(
	for Tag in "${Tags[@]}"; do
		echo "v${Tag}::git+${url}#tag=v${Tag}"
	done
)


sha1sums=(); readarray -t sha1sums < <(
	for Item in "${Tags[@]}"; do
		echo SKIP
	done
)


if [[ "${#source[@]}" != 0 ]]; then
	source+=(
		"alpm.conf"
		"fortran.conf"
		"makepkg.conf"
		"pacman.conf"
		"rust.conf"
		"skel-pacnews.hook"
	)
fi


if [[ "${#sha1sums[@]}" != 0 ]]; then
	sha1sums+=(
		"8b20540735a49594c0ce5e2c46fc3db5d8034795"
		"f0be704c014b417d96d7729a0250ba4e5682ed16"
		"fd1cfc5b3ede74158e1132e43196decf2a05fa52"
		"671ab1c992ff106c282e32b7ee0d05445da09d34"
		"dd3b71dc3c61107770e442f905f2cef04f7baf97"
		"30fc96fc2a805cb84d5fd9f8b48c62c397e4ddde"
	)
fi


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


build () {
	local Tag

	for Tag in "${Tags[@]}"; do
		BuildTag "${Tag}"
	done
}


BuildTag () {
	local Tag="${1}"
	local Lock="${srcdir}/_BUILD.lock"
	cd "${srcdir}/v${Tag}"

	if [[ ! -f "${Lock}" ]]; then
		so rm --recursive --force build
		ConfigureBuild
		so meson compile -C build 1>&2
	fi
}


ConfigureBuild () {
	so meson build \
		--prefix=/usr \
		--buildtype=plain \
		-D doc=enabled \
		-D doxygen=enabled \
		-D scriptlet-shell=/usr/bin/bash \
		-D ldconfig=/usr/bin/ldconfig
}


check () {
	CheckExecution
	RunTests
}


CheckExecution () {
	so "${srcdir}/v${Tags[0]}/build/pacman" --help
}


RunTests () {
	local Lock="${srcdir}/_BUILD.lock"

	if [[ ! -f "${Lock}" ]]; then
		cd "${srcdir}/v${Tags[0]}"
		so meson test -C build 1>&2
		touch "${Lock}"
	fi
}


package () {
	PackageFiles
	source /usr/lib/AppendLibs
}


PackageFiles () {
	PackageOldLibs
	PackageProgram
	PackageConfigs
	PackageHook
}


PackageOldLibs () {
	local Lib
	local LibsDir="${pkgdir}/usr/lib"

	so mkdir --parents "${LibsDir}"
	cd "${srcdir}/v${Tags[1]}/build"

	while read -r Lib; do
		so cp --no-dereference "${Lib}" "${LibsDir}"
	done < <(
		find . -maxdepth 1 -type f,l -name "lib*.so.*" |
		cut --delimiter='/' --fields=2
	)
}


PackageProgram () {
	cd "${srcdir}/v${Tags[0]}"
	DESTDIR="${pkgdir}" so meson install -C build
	sed --in-place "s|---mirror|--mirror|" "${pkgdir}/usr/share/makepkg/source/git.sh"
}


PackageConfigs () {
	so install -dm755 "${pkgdir}/etc"
	so install -Dm644 "${srcdir}/alpm.conf" "${pkgdir}/usr/lib/sysusers.d/alpm.conf"
	so install -Dm644 "${srcdir}/fortran.conf" "${pkgdir}/etc/makepkg.conf.d/fortran.conf"
	so install -Dm644 "${srcdir}/makepkg.conf" "${pkgdir}/etc"
	so install -Dm644 "${srcdir}/pacman.conf" "${pkgdir}/etc"
	so install -Dm644 "${srcdir}/rust.conf" "${pkgdir}/etc/makepkg.conf.d/rust.conf"
}


PackageHook () {
	so install -m644 "${srcdir}/skel-pacnews.hook" "${pkgdir}/usr/share/libalpm/hooks"
}
