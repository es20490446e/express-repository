#! /bin/bash


post_install () {
	skull plasma-planzen-3
	systemctl enable --now power-profiles-daemon.service
}


post_upgrade () {
	post_install
}

