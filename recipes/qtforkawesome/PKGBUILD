#! /bin/bash

pkgname="qtforkawesome"

epoch=101
pkgrel=1
arch=("x86_64")

pkgdesc="Allows using ForkAwesome within Qt apps"
url="https://github.com/Martchus/qtforkawesome"
license=("GPL")


Tags=(); readarray -t Tags < <(
	repoInfo tags "${url}" |
	cut --delimiter='v' --fields=2- |
	head -n2
)


QtVersion="$(
	pacman --sync --info "qtutilities" |
	grep '^Version' |
	cut --delimiter='t' --fields=2 |
	cut --delimiter='v' --fields=1 |
	head -n1
)"


pkgver="qt${QtVersion}v${Tags[0]}"


makedepends=(
	"clang"
	"cmake"
	"git"
	"ninja"
	"perl-yaml-libyaml"
	"qtutilities"
	"qt${QtVersion}-declarative"
)


depends=(
	"qt${QtVersion}-base"
)


source=(
	"git+${url}"
    "fork::git+https://github.com/ForkAwesome/Fork-Awesome"
)


sha1sums=(
	"SKIP"
	"SKIP"
)


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


build () {
	local Tag

	for Tag in "${Tags[@]}"; do
		BuildTag "${Tag}"
	done
}


BuildTag () {
	local Tag="${1}"
	local Lock="${srcdir}/v${Tag}.lock"

	if [[ ! -f "${Lock}" ]]; then
		ChangeIntoTag "${Tag}"
		ConfigureBuild "${Tag}"
		so ninja
	fi
}


ChangeIntoTag () {
	local Tag="${1}"

	so rm --recursive --force "${srcdir}/v${Tag}"
	so cp --recursive "${srcdir}/${pkgname}" "${srcdir}/v${Tag}"
	cd "${srcdir}/v${Tag}"
	so git checkout "tags/v${Tag}"
}


ConfigureBuild () {
	so cmake \
		-G Ninja \
		-D BUILD_SHARED_LIBS:BOOL=ON \
		-D BUILTIN_TRANSLATIONS:BOOL=ON \
		-D CMAKE_BUILD_TYPE:STRING="Release" \
		-D CMAKE_INSTALL_PREFIX:PATH="/usr" \
		-D CONFIGURATION_DISPLAY_NAME="" \
		-D CONFIGURATION_NAME:STRING="${QtVersion}" \
		-D CONFIGURATION_PACKAGE_SUFFIX="-$(DependMajorVersion "c++utilities")" \
		-D CONFIGURATION_PACKAGE_SUFFIX_QTUTILITIES:STRING="-$(DependMajorVersion "qtutilities")" \
		-D CONFIGURATION_TARGET_SUFFIX:STRING="${QtVersion}" \
		-D FORK_AWESOME_FONT_FILE="${srcdir}/fork/fonts/forkawesome-webfont.woff2" \
		-D FORK_AWESOME_ICON_DEFINITIONS="${srcdir}/fork/src/icons/icons.yml" \
		-D KF_PACKAGE_PREFIX:STRING="KF${QtVersion}" \
		-D QT_PACKAGE_PREFIX:STRING="Qt${QtVersion}" \
		.
}


DependMajorVersion () {
	local Depend="${1}"

	grep "${Depend}" "CMakeLists.txt" |
	tr ' ' '\n' |
	grep "[[:digit:]]" |
	cut --delimiter='.' --fields=1
}


check () {
	cd "${srcdir}/v${Tags[0]}"

	if ! QT_QPA_PLATFORM="offscreen" so ninja check; then
		so rm --force "${srcdir}/"*.lock
		false
	fi
}


package () {
	PackageTags
	source "/usr/lib/AppendLibs"
}


PackageTags () {
	PackageTag "${Tags[1]}"
	PackageTag "${Tags[0]}"
}


PackageTag () {
	local Tag="${1}"

	cd "${srcdir}/v${Tag}"
	DESTDIR="${pkgdir}" so ninja install
}
