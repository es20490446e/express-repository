#! /bin/bash


post_install () {
	RemoveOldIntelGraphicsDriver
}


RemoveOldIntelGraphicsDriver () {
	if pacman --query --info xf86-video-intel &>/dev/null; then
		rm --force "/var/lib/pacman/db.lck"
		pacman --remove --recursive --noconfirm xf86-video-intel
		touch "/var/lib/pacman/db.lck"
	fi
}
