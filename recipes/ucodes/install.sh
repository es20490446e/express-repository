#! /bin/bash


post_install () {
	UseMicrocodeHook
}


post_upgrade () {
	UseMicrocodeHook
}


pre_remove () {
	RemoveMicrocodeHook
}


UseMicrocodeHook () {
	addMicrocodeHook
	removeMicrocodeOption
}


addMicrocodeHook () {
	local Conf="/etc/mkinitcpio.conf"

	if [[ -f "${Conf}" ]] && ! grep --quiet "microcode" "${Conf}"; then
		sed --in-place --regexp-extended \
			's|^HOOKS=\((.*)autodetect|HOOKS=\(\1autodetect microcode|' \
			"${Conf}"
	fi
}


removeMicrocodeOption () {
	local Dir="/etc/mkinitcpio.d"
	local Preset

	if [[ -d "${Dir}" ]]; then
		while read -r Preset; do
			sed --in-place \
				'/_microcode/d' \
				"${Preset}"
		done < <(
			if [[ -d "${Dir}" ]]; then
				find "${Dir}" -name "*\.preset"
			fi
		)
	fi
}


RemoveMicrocodeHook () {
	local Conf="/etc/mkinitcpio.conf"

	if [[ -f "${Conf}" ]]; then
		sed --in-place --regexp-extended \
			's|^HOOKS=\((.*)microcode |HOOKS=\(\1|' \
			"${Conf}"
	fi
}
