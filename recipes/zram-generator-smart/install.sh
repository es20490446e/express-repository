#! /bin/bash
set -e


post_install () {
	systemctl daemon-reload
	Zram stop
	Zram start
}


post_upgrade () {
	post_install
}


pre_remove () {
	Zram stop
}


post_remove () {
	systemctl daemon-reload
}


Devices () {
	local Conf="/etc/systemd/zram-generator.conf"

	if [[ -f "${Conf}" ]]; then
		grep --extended-regexp "^\[zram[[:digit:]]+\]$" "${Conf}" |
		tr -d "[]"
	fi
}


Zram () {
	local Action="${1}"
	local Device
	local Devices; readarray -t Devices < <(Devices)

	for Device in "${Devices[@]}"; do
		systemctl "${Action}" "systemd-zram-setup@${Device}.service"
	done
}
